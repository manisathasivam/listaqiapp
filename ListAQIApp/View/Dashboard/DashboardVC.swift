//
//  ViewController.swift
//  ListAQIApp
//
//  Created by MANI on 2/6/22.
//

import UIKit

class DashboardVC: UITableViewController {
    let apiService = SocketService()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register cell
        tableView.register(AQIDisplayCell.nib(), forCellReuseIdentifier: AQIDisplayCell.cellID)
        
        // Initiate process to get AQI data
        apiService.delegate = self
        apiService.fetchAQIList()
    }
}

// Display AQI data
extension DashboardVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DashboardViewModel.shared.aqiDataList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = self.tableView.dequeueReusableCell(withIdentifier: Cells.aqiDisplayCell) as? AQIDisplayCell {
            header.lblCity.text = "CITY"
            header.lblCity.backgroundColor = .clear
            header.lblCity.font = UIFont.boldSystemFont(ofSize: 25.0)
            header.lblAQI.text = "Current AQI"
            header.lblAQI.backgroundColor = .clear
            header.lblAQI.font = UIFont.boldSystemFont(ofSize: 25.0)
            header.lblLastUpdated.text = "Last Updated"
            header.lblLastUpdated.font = UIFont.boldSystemFont(ofSize: 25.0)
            header.lblLastUpdated.backgroundColor = .clear
            header.contentView.backgroundColor = .lightGray
            return header.contentView
        }
        
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: Cells.aqiDisplayCell, for: indexPath) as? AQIDisplayCell {
            cell.aqiData = DashboardViewModel.shared.aqiDataList[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AQIDetailsVC") as? AQIDetailsVC {
            vc.selectedAQIData = DashboardViewModel.shared.aqiDataList[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// Delegate mothod to receive data from socket
extension DashboardVC: GetAQIServiceProtocol {
    func receiveAQIDataWith(_ isSuccess: Bool, data: [AQIData]?) {
        if isSuccess {
            DashboardViewModel.shared.prepareData(aqiData: data ?? [])
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
