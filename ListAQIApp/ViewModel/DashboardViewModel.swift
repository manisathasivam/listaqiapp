//
//  DataManager.swift
//  Sample
//
//  Created by MANI on 2/6/22.
//

import Foundation

class DashboardViewModel {
    
    // Singleton class to share AQI data across screens
    static let shared = DashboardViewModel()
    var aqiDataList = [AQIData]()
    
    // Restrict to init again
    private init(){}

    // Prepare display data
    func prepareData(aqiData: [AQIData]) {
        for item in aqiData {
            if let _ = DashboardViewModel.shared.aqiDataList.first(where: {$0.city == item.city}) {
                DashboardViewModel.shared.aqiDataList = DashboardViewModel.shared.aqiDataList.map {
                    var mutable = $0
                    if $0.city == item.city {
                        mutable.aqi = item.aqi
                        mutable.lastupdated = Date()
                    }
                    return mutable
                }
            } else {
                DashboardViewModel.shared.aqiDataList.append(item)
            }
        }
    }

}

