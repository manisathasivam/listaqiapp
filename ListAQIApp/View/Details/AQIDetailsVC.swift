//
//  AQIDetailsVC.swift
//  Sample
//
//  Created by MANI on 2/6/22.
//

import UIKit
import CorePlot

class AQIDetailsVC: UIViewController {
    
    var plotData = [Double]()
    var plot: CPTScatterPlot!
    var maxDataPoints = 100
    var timer : Timer?
    var currentIndex: Int!
    var timeDuration:Double = 0.3
    var selectedAQIData: AQIData?

    var yMin = 0.0
    var yMax = 500.0
    @IBOutlet var hostView: CPTGraphHostingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure chart layout
        configureGraphtView()
        
        // Set Y max and Min Value
        yMin = (selectedAQIData?.aqi ?? 100.0) - 100
        yMax = (selectedAQIData?.aqi ?? 400.0) + 100
        
        // Configure X&Y
        configureGraphAxis(yMin, yMax: yMax)
        
        //Configure plot line
        configurePlot()
    }
    
    func resetGraphView() {
        configureGraphtView()
        configureGraphAxis(0.0, yMax: 500.0)
        configurePlot()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: self.timeDuration, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
    }
    
    @objc func fireTimer(){
        let graph = self.hostView.hostedGraph
        let plot = graph?.plot(withIdentifier: "mindful-graph" as NSCopying)
        if((plot) != nil){
           if(self.plotData.count >= maxDataPoints){
               self.plotData.removeFirst()
               plot?.deleteData(inIndexRange:NSRange(location: 0, length: 1))
           }
        }
        guard let plotSpace = graph?.defaultPlotSpace as? CPTXYPlotSpace else { return }

        let location: NSInteger
        if self.currentIndex >= maxDataPoints {
           location = self.currentIndex - maxDataPoints + 2
        } else {
           location = 0
        }

        let range: NSInteger

        if location > 0 {
           range = location-1
        } else {
           range = 0
        }

        let oldRange =  CPTPlotRange(locationDecimal: CPTDecimalFromDouble(Double(range)), lengthDecimal: CPTDecimalFromDouble(Double(maxDataPoints-2)))
        let newRange =  CPTPlotRange(locationDecimal: CPTDecimalFromDouble(Double(location)), lengthDecimal: CPTDecimalFromDouble(Double(maxDataPoints-2)))

        CPTAnimation.animate(plotSpace, property: "xRange", from: oldRange, to: newRange, duration:0.3)

        self.currentIndex += 1
        var point = self.selectedAQIData?.aqi ?? 0.0
        if let value = DashboardViewModel.shared.aqiDataList.filter({$0.city == self.selectedAQIData?.city}).first {
            point = value.aqi
        }
        if yMin > point || yMax < point {
            self.resetGraphView()
        }
        self.plotData.append(point)
        plot?.insertData(at: UInt(self.plotData.count-1), numberOfRecords: 1)
    }
    
    func configureGraphtView(){
        hostView.allowPinchScaling = false
        self.plotData.removeAll()
        self.currentIndex = 0
    }
    
    func configureGraphAxis(_ yMin: Double, yMax: Double){
        
        let graph = CPTXYGraph(frame: hostView.bounds)
        graph.plotAreaFrame?.masksToBorder = false
        hostView.hostedGraph = graph
        graph.backgroundColor = UIColor.black.cgColor
        graph.paddingBottom = 40.0
        graph.paddingLeft = 40.0
        graph.paddingTop = 30.0
        graph.paddingRight = 15.0
        

        //Set title for graph
        let titleStyle = CPTMutableTextStyle()
        titleStyle.color = CPTColor.white()
        titleStyle.fontName = "HelveticaNeue-Bold"
        titleStyle.fontSize = 20.0
        titleStyle.textAlignment = .center
        graph.titleTextStyle = titleStyle

        let title = "CorePlot"
        graph.title = title
        graph.titlePlotAreaFrameAnchor = .top
        graph.titleDisplacement = CGPoint(x: 0.0, y: 0.0)
        
        let axisSet = graph.axisSet as! CPTXYAxisSet
        
        let axisTextStyle = CPTMutableTextStyle()
        axisTextStyle.color = CPTColor.white()
        axisTextStyle.fontName = "HelveticaNeue-Bold"
        axisTextStyle.fontSize = 10.0
        axisTextStyle.textAlignment = .center
        let lineStyle = CPTMutableLineStyle()
        lineStyle.lineColor = CPTColor.white()
        lineStyle.lineWidth = 5
        let gridLineStyle = CPTMutableLineStyle()
        gridLineStyle.lineColor = CPTColor.gray()
        gridLineStyle.lineWidth = 0.5
       

        if let x = axisSet.xAxis {
            x.majorIntervalLength   = 20
            x.minorTicksPerInterval = 20
            x.labelTextStyle = axisTextStyle
            x.minorGridLineStyle = gridLineStyle
            x.axisLineStyle = lineStyle
            x.axisConstraints = CPTConstraints(lowerOffset: 0.0)
            x.delegate = self
        }

        if let y = axisSet.yAxis {
            y.majorIntervalLength   = 40
            y.minorTicksPerInterval = 40
            y.minorGridLineStyle = gridLineStyle
            y.labelTextStyle = axisTextStyle
            y.alternatingBandFills = [CPTFill(color: CPTColor.init(componentRed: 255, green: 255, blue: 255, alpha: 0.03)),CPTFill(color: CPTColor.black())]
            y.axisLineStyle = lineStyle
            y.axisConstraints = CPTConstraints(lowerOffset: 0.0)
            y.delegate = self
        }

        // Set plot space
        let xMin = 0.0
        let xMax = 100.0
        let yMin = yMin
        let yMax = yMax
        guard let plotSpace = graph.defaultPlotSpace as? CPTXYPlotSpace else { return }
        plotSpace.xRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(xMin), lengthDecimal: CPTDecimalFromDouble(xMax - xMin))
        plotSpace.yRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(yMin), lengthDecimal: CPTDecimalFromDouble(yMax - yMin))
        
    }
    
    func configurePlot(){
        plot = CPTScatterPlot()
        let plotLineStile = CPTMutableLineStyle()
        plotLineStile.lineJoin = .round
        plotLineStile.lineCap = .round
        plotLineStile.lineWidth = 2
        plotLineStile.lineColor = CPTColor.white()
        plot.dataLineStyle = plotLineStile
        plot.curvedInterpolationOption = .catmullCustomAlpha
        plot.interpolation = .curved
        plot.identifier = "mindful-graph" as NSCoding & NSCopying & NSObjectProtocol
        guard let graph = hostView.hostedGraph else { return }
        plot.dataSource = self
        plot.delegate = self
        graph.add(plot, to: graph.defaultPlotSpace)
    }

}

extension AQIDetailsVC: CPTScatterPlotDataSource, CPTScatterPlotDelegate {
    func numberOfRecords(for plot: CPTPlot) -> UInt {
        return UInt(self.plotData.count)
    }

    func number(for plot: CPTPlot, field: UInt, record: UInt) -> Any? {
       switch CPTScatterPlotField(rawValue: Int(field))! {
            case .X:
                return NSNumber(value: Int(record) + self.currentIndex-self.plotData.count)
            case .Y:
                return self.plotData[Int(record)] as NSNumber
            default:
                return 0
        }
    }
}
