//
//  SocketService.swift
//  ListAQIApp
//
//  Created by MANI on 2/6/22.
//

import Foundation

// Protocal to pass the value after socket receives the value
protocol GetAQIServiceProtocol {
    func receiveAQIDataWith(_ isSuccess: Bool, data: [AQIData]?)
}

class SocketService: NSObject, URLSessionWebSocketDelegate  {
    var webSocket: URLSessionWebSocketTask?
    var delegate: GetAQIServiceProtocol?
    
    func fetchAQIList() {
        let session = URLSession.init(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        let url = URL.init(string: "wss://city-ws.herokuapp.com")
        webSocket =  session.webSocketTask(with: url!)
        webSocket?.resume()
    }
    
    func ping(){
        webSocket?.sendPing(pongReceiveHandler: { error in
            guard let error = error else{
                return
            }
            print("ping error\(error)")
        })
    }
    
    func close(){
        webSocket?.cancel(with: .goingAway, reason: "end".data(using: .utf8))
    }
    
    func send(){
        DispatchQueue.global().asyncAfter(deadline: .now()+2) {
            self.webSocket?.send(.string(Strings.socketMessage), completionHandler: { error in
                guard let error = error else{
                    self.send()
                    return
                }
                print("ping error\(error)")
            })
        }
    }
    func receive(){
        webSocket?.receive(completionHandler: {[weak self] result in
            switch result{
            case .success(let message):
                switch message {
                case .data(let data):
                    print("Got Data\(data)")
                case .string(let message):
                    if "\(Strings.serverResponseForNoData)\(Strings.socketMessage)" == message {
                        //print("NoUpdate Available")
                    } else {
                        do{
                            if let json = message.data(using: String.Encoding.utf8) {
                                let resultData = try JSONDecoder().decode([AQIData].self, from: json)
                                self?.delegate?.receiveAQIDataWith(true, data: resultData)
                            }
                        }catch {
                            self?.delegate?.receiveAQIDataWith(false, data: nil)
                        }
                    }
                @unknown default:
                    break
                }
            case .failure(let error):
                print("Got error\(error)")
                self?.delegate?.receiveAQIDataWith(false, data: nil)
            }
            self?.receive()
        })
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        self.ping()
        self.receive()
        self.send()
    }
    
    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        print("Did close socket \(reason?.base64EncodedString() ?? "")")
    }


}
