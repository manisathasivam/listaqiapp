
# MVVM iOS SWift


Architecture used: MVVM

    - Initial View will be loaded with help of DashboardVC.swift. It will trigger socket connection and it confirmes deleget to get the data back.
    - After URL session initialize we have to call web socket Task and passing of the URL that is provided.
    - Methods like ping/close/send/receive will help to deal with communicating to socket server.
    - Session delegate method will help to trigger the above methods.
    - Once we get the data in receive method we will pass that back to view with help of delegate method that we confirmed in view controller.
    - Fianlly Will prepare the screen data and reload our table view.
    - When user click on any of the item it will bring user to details screen with graph. Graph will be refreshed every 30 minutes and will drow line based on the value.
    
Reason to use MVVM: Mainly to seperate the data handling part(viewmodel). 


# Pod Used
    CorePlot : To Show the AQI data in graph 


# screen shots
https://bitbucket.org/manisathasivam/listaqiapp/src/master/Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202022-02-06%20at%2021.14.08.png
https://bitbucket.org/manisathasivam/listaqiapp/src/master/Simulator%20Screen%20Shot%20-%20iPhone%2011%20Pro%20Max%20-%202022-02-06%20at%2021.14.20.png

# Project Overview

* DashboardVC.swift - Dashboard class 
* AQIDetailsVC.swift - Details screen class
* DashboardViewModel.swift      - View model class
* SocketService.swift           - Api Servie class 
