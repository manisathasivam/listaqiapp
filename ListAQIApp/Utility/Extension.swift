//
//  Extension.swift
//  ListAQIApp
//
//  Created by MANI on 2/6/22.
//

import Foundation

extension Date {
    func toString(withFormat format: String = "EEEE ، d MMMM yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)
        return str
    }
}

