//
//  AQIData.swift
//  Sample
//
//  Created by MANI on 2/6/22.
//

import Foundation


struct AQIData: Codable  {
    let city: String
    var aqi: Double
    var lastupdated: Date?
    private enum CodingKeys: String, CodingKey {
        case city
        case aqi
        case lastupdated
    }
    init(city: String, aqi: Double) {
        self.city = city
        self.aqi = aqi
        self.lastupdated = Date()
    }
}
