//
//  AQIDisplayCell.swift
//  Sample
//
//  Created by MANI on 2/6/22.
//

import UIKit

class AQIDisplayCell: UITableViewCell {

    static let cellID = Cells.aqiDisplayCell
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblAQI: UILabel!
    @IBOutlet weak var lblLastUpdated: UILabel!
    
    var aqiData: AQIData! {
        didSet{
            self.lblCity.text = aqiData.city
            self.lblAQI.textColor = self.getAQIBGColor(aqiData.aqi)
            self.lblAQI.text = String(format: "%.2f", aqiData.aqi)
            let dateValue = self.setLastUpdatedDate(aqiData.lastupdated ?? Date(), rhs: Date())
            self.lblLastUpdated.text = dateValue.titeStatus
            self.lblLastUpdated.backgroundColor = dateValue.color
        }
    }
    
    // method to get lastupdated date along with color code
    func setLastUpdatedDate(_ lhs: Date, rhs: Date) -> (titeStatus: String, color: UIColor) {
        let diffComponents = Calendar.current.dateComponents([.minute, .second], from: lhs, to: rhs)
        let minutes = diffComponents.minute ?? 0
        let seconds = diffComponents.second ?? 0
        if minutes == 0 && seconds < 60 {
            return (titeStatus: "Few secounds ago", color: UIColor.green)
        } else if minutes < 5 {
            return (titeStatus: "Few minutes ago", color: UIColor.orange)
        } else {
            return (titeStatus: "\(lhs.toString(withFormat: "HH:MM a")) ago", color: UIColor.red)
        }
    }
    
    // method to get color code for aqi data based on AQI value
    func getAQIBGColor(_ aqiValue: Double) -> UIColor{
        switch aqiValue{
        case 0...50:
            return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case 51...100:
            return #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)

        case 101...200:
            return #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)

        case 201...300:
            return #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)

        case 301...400:
            return #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        case 401...500:
            return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        default:
            return UIColor.red
        }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "AQIDisplayCell", bundle: nil)
    }
}
